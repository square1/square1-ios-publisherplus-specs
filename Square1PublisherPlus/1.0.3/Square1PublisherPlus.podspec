Pod::Spec.new do |s|

  s.name          = "Square1PublisherPlus"
  s.version       = "1.0.3"
  s.summary       = "The iOS Publisher Plus SDK"
  s.description   = "The iOS Publisher Plus SDK with all the Publisher Plus client logic"
  s.homepage      = "https://bitbucket.org/square1/square1-ios-publisherplus"
  s.author        = "Square1"
  s.platform      = :ios
  s.platform      = :ios, "10.0"
  s.source        = { :git => "git@bitbucket.org:square1/square1-ios-publisherplus.git", :tag => s.version }
  s.source_files  = "Source/**/*.swift"
  s.resources     = "Source/**/*.{xcdatamodeld,xib,storyboard,xcassets}"

  s.dependency 'Square1Tools'
  s.dependency 'Square1CoreData'
  s.dependency 'Square1Network'
  s.dependency 'Square1Media'
  s.dependency 'Kingfisher'
  s.dependency 'TwitterKit'
  s.dependency 'youtube-ios-player-helper'
  s.dependency 'hpple'
end